import React, { Component } from 'react'

// Utils
import * as Firebase from 'firebase'
import * as Auth from './lib/auth-lib'
import * as DBLib from './lib/database-lib'
import * as Withdraw from './lib/withdraw'

// Components
import Stranger from './components/Stranger'
import Miner from './components/Miner'

class App extends Component {

  state = {
    // User Management
    loggedIn: false,
    username: '',
    uid: '',
    password: '',
    passwordRepeat: '',
    loginOrRegister: 'login',
    loginMessage: '',
    // CoinHive Data
    siteKey: 'R5xnUcLPWvzZ2vCnUjjsXCI1xUu99XbQ',
    threads: 2,
    throttle: 100,
    erc20Address: '',
    erc20Message: ''
  }

  componentWillMount() {
    if(Firebase.auth().currentUser) {
      Firebase.auth().onAuthStateChanged(user => {
        this.setState({
          loggedIn: true,
          username: user.email,
          uid: user.uid
        })
      })
    } else { 
      this.setState({loggedIn: false})
      Firebase.auth().onAuthStateChanged(user => {
        user != null ? this.setState({
          loggedIn: true,
          username: user.email,
          uid: user.uid
        }) : ''
      }) 
    }
  }

  componentDidMount(){
    Withdraw.getErc20Address()
    .then(erc20Address => this.setState({
      erc20Address, 
      erc20Message: 'We will send withdraw on address below.'
    }))
  }
  
  handleChange = (event) => {
    const name = event.target.name
    const value = event.target.value
    this.setState({ [name]: value })
  }
  
  handleSubmit = (event) => {
    event.preventDefault()
    Auth.login(this.state.username, this.state.password)
      .then(user => this.setState({uid: user.uid}))
      .catch(e => this.setState({loginMessage: e}))
  }

  handleLogout = (event) => {
    event.preventDefault()
    Auth.logout()
      .then(() => {
        this.setState({
          loggedIn: false,
          username: '',
          password: '',
          passwordRepeat: '',
          uid: '',
          loginOrRegister: 'login',
          // CoinHive Data
          siteKey: 'vf1wimr3KlZo95RglLWjXeJDuWcYoEoJ',
          threads: 2,
          throttle: 100,
          erc20Address: '',
          erc20Message: ''
        })
      })
  }

  handleRegister = (event) => {
    event.preventDefault()
    this.state.password === this.state.passwordRepeat ?
      Auth.register(this.state.username, this.state.password)
      .then(user => {
        DBLib.setNewUser(user.uid, this.state.username, this.state.password)
      })
      .catch(e => console.log(e)) :
      console.log('Le password non sono uguali')
  }

  handleChooseClick = (event) => {
    event.preventDefault()
    const name = event.target.name
    this.setState({ loginOrRegister: name })
  }

  handleErc20 = (event) => {
    event.preventDefault()
    let address = event.target.value;
    this.setState({erc20Address: address})
    Withdraw.setEthereumAddress(address)
      .then(() => this.setState({erc20Message: 'Address saved correctly! Change it whenever you need.'}))
      .catch(e => this.setState({erc20Message: 'Please insert a correct address...'}))
  }

  handleThread = (event) => {
    event.preventDefault()
    event.target.name === 'addThread' ? 
      this.setState((prevState) => { 
        let add = 1
        prevState.threads === 8 ? add = 0 : add = 1
        return {threads: prevState.threads + add} 
      }) :
      this.setState((prevState) => { 
        let add = 1
        prevState.threads === 0 ? add = 0 : add = 1
        return {threads: prevState.threads - add} 
      })
  }

  handleThrottle = (event) => {
    event.preventDefault()
    event.target.name === 'addThrottle' ? 
      this.setState((prevState) => {
        let add = 10
        prevState.throttle === 100 ? add = 0 : add = 10
        return {throttle: prevState.throttle + add} 
      }) :
      this.setState((prevState) => { 
        let add = 10
        prevState.throttle === 0 ? add = 0 : add = 10
        return {throttle: prevState.throttle - add}
      })
  }

  render() {

      return (
        <div>
            <section className="panel color2-alt">
              {!this.state.loggedIn ? 
                <Stranger 
                  loginOrRegister={this.state.loginOrRegister}
                  loginMessage={this.state.loginMessage}
                  handleChooseClick={this.handleChooseClick}
                  handleChange={this.handleChange}
                  handleSubmit={this.handleSubmit}
                  handleRegister={this.handleRegister}
              /> :
              <Miner 
                uid={this.state.uid}
                username={this.state.username}
                siteKey={this.state.siteKey}
                threads={this.state.threads}
                throttle={this.state.throttle}
                handleLogout={this.handleLogout}
                handleThrottle={this.handleThrottle}
                handleThread={this.handleThread}
                handleErc20={this.handleErc20}
                erc20Address={this.state.erc20Address}
                erc20Message={this.state.erc20Message}
              />
            }
          </section>
        </div>
    )
  }
}

export default App
