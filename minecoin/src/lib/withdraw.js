import * as EthereumValidator from './ethereum-address/index'
import * as Firebase from 'firebase'
import Axios from 'axios'

export const setEthereumAddress = (address) => {
    return new Promise((resolve, reject) => {
        if(EthereumValidator.isAddress(address)) {
            Firebase.auth().onAuthStateChanged(user => {
                user != null ?
                Firebase.database().ref('/users/' + user.uid + '/erc20Address').set(address)
                    .then(() => resolve())
                    .catch(e => console.log(e)) : resolve()
            })
        } else {
            reject('Invalid erc20 address')
        }
    })
}

export const getErc20Address = () => {
    return new Promise((resolve, reject) => {
        Firebase.auth().onAuthStateChanged(user => {
            user != null ?
            Firebase.database().ref('/users/' + user.uid + '/erc20Address')
            .once('value', (snapshot) => {
                snapshot.val() == null ? resolve('') : resolve(snapshot.val())
            }) : resolve('')
        })
    })
}

// export const sendEmailToNotifyWithdraw = (uid) => {
//     return new Promise((resolve, reject) => {
//         emailPush().then(key => {
//             let url = 'https://us-central1-minecoin-d90fb.cloudfunctions.net/alertWithdraw/' + uid + '/' + key
//             Axios.get(url)
//                 .then(res => {
//                     resolve(res)
//                 })
//                 .catch(e => reject(e))
//         }).catch(e => console.log(e))
//     })
// }

// Push a new record at /users/uid/cashout/{pushKey} -> false = pending... / true = done
export const setNewCashout = () => {
    return new Promise((resolve, reject) => {
        Firebase.auth().onAuthStateChanged(user => {
            const checkOutData = {
                time: Date.now(),
                status: false
            }
            const key = Firebase.database().ref('/users/' + user.uid + '/cashout').push().key
            Firebase.database().ref('/users/' + user.uid + '/cashout').child(key).set(checkOutData)
                .then(() => notifyCashout(user.uid).then(() => resolve))
                .catch(e => reject(e))
        })
    })
}

const notifyCashout = (uid) => {
    return new Promise((resolve, reject) => {
        Axios.get('https://us-central1-minecoin-d90fb.cloudfunctions.net/alertWithdraw/' + uid)
        .then(response => resolve(true))
        .catch(e => reject(e))
    })
}

export const getLastCashoutStatus = () => {
    return new Promise((resolve, reject) => {
        Firebase.auth().onAuthStateChanged(user => {
            Firebase.database().ref('/users/' + user.uid + '/cashout')
                .limitToLast(1).once('value', (snapshot) => {
                    const val = snapshot.val()
                    if(val !== null) {
                        for (let key of Object.keys(val)) {
                            resolve(val[key].status)
                        }
                    } else { resolve(true) }
            })
        })
    })
}

// const setLastCashoutDone = () => {
//     return new Promise((resolve, reject) => {
//         Firebase.auth().onAuthStateChanged(user => {
//             Firebase.database().ref('/users/' + user.uid + '/cashout')
//                 .limitToLast(1).once('value', (snapshot) => {
//                     let val = snapshot.val()
//                     for(let key of Object.keys(val)) {
//                         Firebase.database().ref('/users/' + user.uid + '/cashout/' + key).child('status').set(true)
//                             .then(resolve())
//                             .catch(e => reject(e))
//                     }
//                 })
//         })
//     })
// }

// const Firebase = require('./firebase-conf')
// const Axios = require('axios')
// Firebase.auth().signInWithEmailAndPassword('lippolis94@me.com', 'nonnavera')
//     .then(() => {
//         setNewCashout().then(key => console.log(key))
//         // checkLastCashout().then(val => console.log(val))
//         // setLastCashoutDone().then(() => console.log('done')).catch(e => console.log(e))
//     })