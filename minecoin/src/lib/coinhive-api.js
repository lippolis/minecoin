const axios = require('axios')

const userBalance = (user) => {
    return new Promise((resolve, reject) => {
        let url = 'https://us-central1-minecoin-d90fb.cloudfunctions.net/getBalance/' + user
        axios.get(url)
            .then(res => resolve(res.data))
            .catch(e => reject(e))
    })
}

module.exports.userBalance = userBalance