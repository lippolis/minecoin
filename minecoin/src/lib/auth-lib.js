
const fire = require('./firebase-conf');

export const login = (email, password) => {
    return new Promise((resolve, reject) => {
        fire.auth().signInWithEmailAndPassword(email, password)
            .then(user => resolve(user))
            .catch(e => { 
                reject(e.message)
                console.log(e.code)
            })
    })
}

export const register = (email, password) => {
    return new Promise((resolve, reject) => {
        fire.auth().createUserWithEmailAndPassword(email, password)
            .then(user => resolve(user))
            .catch(e => {
                reject(e.message)
                console.log(e.code)
            })
    })
}

export const logout = () => {
    return new Promise((resolve, reject) => {
        fire.auth().signOut()
            .then(() => resolve())
            .catch(e => reject(e))
    })
}