const fire = require('./firebase-conf')

const db = fire.database()

const getSnapshotVal = (ref) => {
    return new Promise((resolve, reject) => {
        db.ref(ref).once('value', (snapshot) => {
            snapshot.val() == null ? reject('No data here') : resolve(snapshot.val())
        })
    })
}

const setNewUser = (uid, email, password) => {
    return new Promise((resolve, reject) => {
        let data = {
            email: email,
            password: password,
            registrationDate: Date.now()
        }
        db.ref('users/' + uid).set(data)
            .then(() => resolve())
            .catch(e => reject(e))
    })
}

const registerSessionToUser = (uid, sessID) => {
    return new Promise((resolve, reject) => {
        getSnapshotVal('users/' + uid + '/sessions')
            .then(val => {
                let data = val;
                data.push(sessID)
                db.ref('users/' + uid + '/sessions').set(data)
                    .then(() => resolve())
                    .catch(e => reject())
            })
            .catch(() => {
                db.ref('users/' + uid + '/sessions').set([sessID])
                    .then(() => resolve())
                    .catch(e => reject())
            })
    })
}

const startNewSession = (uid) => {
    return new Promise((resolve,reject) => {
        let sessionData = {
            startedAt: Date.now()
        }
        let key = db.ref('sessions').push(sessionData).key
        registerSessionToUser(uid, key)
            .then(() => resolve())
            .catch(e => reject())
    })
}

module.exports = {getSnapshotVal, setNewUser, registerSessionToUser, startNewSession}