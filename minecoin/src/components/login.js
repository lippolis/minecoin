import React from 'react'

const Login = (props) => {
    return (
        <div>
            {props.loginMessage}
            <form onSubmit={props.handleSubmit}>
                <div className="field">
                    <label>Email</label>
                    <input type="text" name="username" id="username" value={props.username} onChange={props.handleChange} />
                </div>
                <div className="field">
                    <label>Password</label>
                    <input type="password" name="password" id="password" value={props.password} onChange={props.handleChange}/>
                </div>
                <ul className="actions">
                    <li><input type="submit" value="Login" className="button special" /></li>
                </ul>
            </form>
        </div>
    )
}

export default Login