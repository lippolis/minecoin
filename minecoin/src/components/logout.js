import React from 'react'

const Logout = (props) => {
    return (
        <div>
            <form onSubmit={props.handleSubmit}>
                <ul className="actions">
                    <li><input type="submit" value="Logout" className="button special" /></li>
                </ul>
            </form>
        </div>
    )
}

export default Logout