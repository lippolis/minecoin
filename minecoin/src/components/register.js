import React from 'react'

const Register = (props) => {
    return (
        <div>
            <form onSubmit={props.handleSubmit}>
                <div className="field">
                    <label>Email</label>
                    <input type="text" name="username" id="username" value={props.username} onChange={props.handleChange} />
                </div>
                <div className="field half">
                    <label>Passowrd</label>
                    <input type="password" name="password" id="password" value={props.password} onChange={props.handleChange}/>
                </div>
                <div className="field half">
                    <label>Repeat Password</label>
                    <input type="password" name="passwordRepeat" id="password" value={props.passwordRepeat} onChange={props.handleChange}/>
                </div>
                <ul className="actions">
                    <li><input type="submit" value="Register" className="button special" /></li>
                </ul>
            </form>
        </div>
    )
}

export default Register