import React from 'react'
import Logout from './logout'

const Settings = (props) => {
    return (
        <div className="span-2-25">
            <h1 className="major"> Settings </h1>
            Set the preferred level of CPU performance and speed
            <ul className="actions">
                <li>
                    <h4>Threads {props.threads}</h4>
                    <ul className="actions">
                        <li><a onClick={props.handleThread} name="subThread" className="button">-</a></li>
                        <li><a onClick={props.handleThread} name="addThread" className="button">+</a></li>
                    </ul>

                </li>
                <li>
                    <h4>Speed {props.throttle}%</h4>
                    <ul className="actions">
                        <li><a onClick={props.handleThrottle} name="subThrottle" className="button">-</a></li>
                        <li><a onClick={props.handleThrottle} name="addThrottle" className="button">+</a></li>
                    </ul>
                </li>
            </ul>
            <h4>Withdrawal</h4>
            <form>
                <div className="field">
                    {props.erc20Message}
                    <input type="text" name="erc20Address" id="demo-name" value={props.erc20Address} placeholder="erc20 address" onChange={props.handleErc20}/>
                </div>
            </form>
            {<Logout handleSubmit={props.handleLogout} />}
        </div>
    )
}

export default Settings