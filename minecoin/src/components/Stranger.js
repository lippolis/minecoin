import React from 'react'

//Components
import Login from './login'
import Register from './register'
import ChooseHowToEnter from './chooseHowToEnter'

export default class Stranger extends React.Component {
    render() {
        return(
            <div className="intro color2-alt">
                <ChooseHowToEnter 
                    loginOrRegister={this.props.loginOrRegister} 
                    handleClick={this.props.handleChooseClick} 
                />
                
                {this.props.loginOrRegister === 'login' ? 
                    <Login handleChange={this.props.handleChange} handleSubmit={this.props.handleSubmit} loginMessage={this.props.loginMessage} /> : null 
                }
                {this.props.loginOrRegister === 'register'? 
                    <Register handleChange={this.props.handleChange} handleSubmit={this.props.handleRegister} /> : null
                }

            </div>
        )
    }
}