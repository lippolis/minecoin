import React from 'react'

const ChooseHowToEnter = (props) => {

    if(props.loginOrRegister === 'login') {
        return (
            <div>
                <ul className="actions">
                    <li>
                        <a name="login" className="button small special" onClick={props.handleClick}>Login</a>
                    </li>
                    <li>
                        <a name="register" className="button small" onClick={props.handleClick}>Register</a>
                    </li>
                </ul>
            </div>
        )
    } else {
        return (
            <div>
            <ul className="actions">
                <li>
                    <a name="login" className="button small" onClick={props.handleClick}>Login</a>
                </li>
                <li>
                    <a name="register" className="button small special" onClick={props.handleClick}>Register</a>
                </li>
            </ul>
        </div>
        )
    }
}

export default ChooseHowToEnter