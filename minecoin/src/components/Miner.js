import React from 'react'
import CoinHive from 'react-coinhive' // https://www.npmjs.com/package/react-coinhive

// Components
import Settings from './settings'

// Lib
import CoinHiveAPI from '../lib/coinhive-api'
import * as Withdraw from '../lib/withdraw'

export default class Miner extends React.Component {

    state = {
        run: false,
        hashesPerSecond: 0,
        settings: false,
        userBalance: {},
        
    }

    componentDidMount() {
        CoinHiveAPI.userBalance(this.props.username)
        .then(res => this.setState({userBalance: res}))
        .catch(e => console.log(e))
        var percentageInterval = setInterval(() => {
            CoinHiveAPI.userBalance(this.props.username)
                .then(res => this.setState({userBalance: res}))
                .catch(e => console.log(e))
        }, 120000)
        this.setState({percentageInterval: percentageInterval})
    }

    componentWillUnmount() {
        this.setState({run: false})
        clearInterval(this.state.minerDataInterval)
        clearInterval(this.state.percentageInterval)
    }

    handleMiner = (miner) => {
        var minerDataInterval = setInterval(() => {
            let data = CoinHive.getMinerData(miner)
            if(this.state.run) {
                console.log(data)
                this.setState({
                    hashesPerSecond: Math.round(data.getHashesPerSecond)
                })
            }
        }, 500)
        this.setState({minerDataIterval: minerDataInterval})
    }

    handleStartStop = (event) => {
        event.preventDefault()
        event.target.name === 'start' ? this.setState({run: true}) : this.setState({run: false})
        event.target.name === 'settings' ? this.setState({settings: true}) : this.setState({settings: false})
    }

    percentageToWithdraw = (hashesCompleted) => {
        let target = 50000000
        let percentage = 0

        typeof(hashesCompleted) === 'number' ? percentage = Math.round((hashesCompleted * 100)/target) : percentage = '...'
        this.cashoutHandler(percentage)
        // return percentage
        let counter = 0
        typeof(hashesCompleted) === 'number' ? counter = hashesCompleted : counter = '...'
        return counter
    }

    cashoutHandler = (percentage) => {
        if(percentage >= 100) {
            Withdraw.getLastCashoutStatus()
                .then(res => {
                    !res ? Withdraw.setNewCashout() : ''
                })
        }
    }

    throttleConversion = (percentage) => {
        if(percentage === 100) {return 0}
        if(percentage === 0) { return 1}
        return ((percentage / 100) - 1).toFixed(2) * -1
    }

    render() {
        return (
            <div className="inner columns aligned">
                <div className="span-2-25">
                    <h1 className="major">Miner</h1>
                    <ul className="actions">
                        <li><a onClick={this.handleStartStop} name="start" className={this.state.run ? 'button circle icon fa-play special' : 'button circle icon fa-play'}><span className="label">Start</span></a></li>
                        <li><a onClick={this.handleStartStop} name="stop" className={!this.state.run ? 'button circle icon fa-pause special' : 'button circle icon fa-pause'}><span className="label">Stop</span></a></li>
                        <li><a onClick={this.handleStartStop} name={this.state.settings ? 'null' : 'settings'} className={this.state.settings ? 'button circle icon fa-cogs special' : 'button circle icon fa-cogs'}><span className="label">Settings</span></a></li>
                    </ul>
                    <p><span className="hashesPerSecond">{this.state.hashesPerSecond} H/s</span></p>
                    <p><span className="hashCompleted">{this.percentageToWithdraw(this.state.userBalance.balance)} Hashes Completed</span></p>
                </div>
                {this.state.settings ? 
                    <Settings 
                        threads={this.props.threads}
                        throttle={this.props.throttle}
                        handleThread={this.props.handleThread}
                        handleThrottle={this.props.handleThrottle}
                        handleErc20={this.props.handleErc20}
                        handleWithdraw={this.props.handleWithdraw}
                        erc20Address={this.props.erc20Address}
                        erc20Message={this.props.erc20Message}
                        handleLogout={this.props.handleLogout}
                    />
                : null}
                <CoinHive
                    userName={this.props.username}
                    siteKey={this.props.siteKey}
                    autoThreads={false}
                    threads={this.props.threads}
                    throttle={this.throttleConversion(this.props.throttle)}
                    src={CoinHive.src.authedmine}
                    run={this.state.run}
                    onInit={miner => this.handleMiner(miner)} 
                />
            </div>
        )
    }
}