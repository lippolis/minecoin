const nodemailer = require('nodemailer');
const functions = require('firebase-functions')
const smtpNodeMailer = require('nodemailer-smtp-transport')

const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;

/*
    Configuration gmail account

    https://accounts.google.com/DisplayUnlockCaptcha
    https://www.google.com/settings/security/lesssecureapps

    firebase functions:config:set gmail.email="minecoinofficial@gmail.com" gmail.password="2121minecoin1212"

    https://github.com/firebase/functions-samples/blob/master/quickstarts/email-users/README.md + nodemailer-smtp-transport
*/

const sendMailToAdminForWithDraw = (erc20Address, userEmail, uid) => {
    return new Promise((resolve, reject) => {
        var transporter = nodemailer.createTransport(smtpNodeMailer({
            service: 'gmail',
            secure: false,
            auth: {
                   user: gmailEmail,
                   pass: gmailPassword
               }
           }));
        
           const mailOptions = {
            from: 'minecoinofficial@gmail.com', // sender address
            to: 'minecoinofficial@gmail.com', // list of receivers
            subject: 'Test invio nodemailer', // Subject line
            html: 'Utente: ' + userEmail + '<br> Address: ' + erc20Address + '<br> Link finito: https://us-central1-minecoin-d90fb.cloudfunctions.net/setTrueToCashout/' + uid  // plain text body
          };
        
          transporter.sendMail(mailOptions, function (err, info) {
            if(err)
              reject(err)
            else
              resolve(info);
         });
    })
}

module.exports = sendMailToAdminForWithDraw