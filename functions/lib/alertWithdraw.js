const express = require('express')
const cors = require('cors')
const admin = require('firebase-admin')
const Email = require('./sendEmail')

var serviceAccount = require("./minecoin-d90fb-firebase-adminsdk-ugbn7-e43cf69697.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://minecoin-d90fb.firebaseio.com"
});

let app = express()

app.use(cors())

app.get('/:uid', (req, res) => { 
    admin.database().ref('/users/' + req.params.uid)
    .once('value', (snaphot) => {
        let userData = snaphot.val()
        Email(userData.erc20Address, userData.email, req.params.uid)
            .then(r => res.send(r))
            .catch(e => res.send(e))
    })
 })

 module.exports = app