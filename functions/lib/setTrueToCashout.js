const express = require('express')
const cors = require('cors')
const admin = require('firebase-admin')

// var serviceAccount = require("./minecoin-d90fb-firebase-adminsdk-ugbn7-e43cf69697.json");
// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: "https://minecoin-d90fb.firebaseio.com"
//   });
  
let app = express()
  
app.use(cors())
  
// Set a cashout record at /users/uid/cashout/{pushKey} -> false = pending... / true = done
app.get('/:uid', (req, res) => { 
      admin.database().ref('/users/' + req.params.uid + '/cashout')
                .limitToLast(1).once('value', (snapshot) => {
                    let val = snapshot.val()
                    for(let key of Object.keys(val)) {
                        admin.database().ref('/users/' + req.params.uid + '/cashout/' + key).child('status').set(true)
                            .then(res.send(true))
                            .catch(e => res.send(false + e))
                    }
            })
   })
  
module.exports = app