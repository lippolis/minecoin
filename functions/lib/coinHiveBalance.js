const axios = require('axios')
const express = require('express')
const cors = require('cors')

const hashesBalance = (user) => {
    return new Promise((resolve, reject) => {
        let secret = 'Tz4DBFjo0JdT4G7OQh7DJFu6qP6pD76n'
        let url = 'https://api.coinhive.com/user/balance?name=' + user + '&secret=' + secret
        // let url = 'https://api.coinhive.com/user/list?secret=' + secret
        axios.get(url)
            .then((res) => {
                resolve(res.data)
            })
            .catch(e => reject(e))
    })
}

let app = express()

app.use(cors())

app.get('/:id', (req, res) => { 
    hashesBalance(req.params.id)
        .then(data => res.send(data))
        .catch(e => res.send(e))
 })

 module.exports = app