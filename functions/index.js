const functions = require('firebase-functions')

//Lib
const getBalance = require('./lib/coinHiveBalance')
const alertWithdraw = require('./lib/alertWithdraw')
const setTrueToCashout = require('./lib/setTrueToCashout')

exports.getBalance = functions.https.onRequest(getBalance)

exports.alertWithdraw = functions.https.onRequest(alertWithdraw)

exports.setTrueToCashout = functions.https.onRequest(setTrueToCashout)